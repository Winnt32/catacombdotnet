﻿using CatacombDotNet.Variables.Graphics;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using CatacombDotNet.Core.Compression;
using CatacombDotNet.Core.Resources;
using CatacombDotNet.Core.Resources.Graphics;
using CatacombDotNet.Implementation.Compression.Huffman;
using CatacombDotNet.Implementation.Resources;
using CatacombDotNet.Implementation.Resources.Graphics;
using Xunit;

namespace CatacombDotNet.Tests
{
    public class EgaGraphicsReaderTest
    {
        private readonly string _egaGraphPath;

        public EgaGraphicsReaderTest()
        {
            _egaGraphPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings.Get("egaGraphPath")));
        }

        private string GetSamplePath(string chunkName)
        {
            return Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), $@"Samples\{chunkName}"));
        }

        [Theory]
        [InlineData(0, 4)]
        [InlineData(1, 383)]
        [InlineData(2, 397)]
        [InlineData(3, 424)]
        [InlineData(4, 1683)]
        [InlineData(159, 219264)]
        [InlineData(160, 220609)]
        [InlineData(161, 223640)]
        public void GettingGraphicsChunkInfo(int chunk, int offset)
        {
            var info = new EgaChunkHeader(Ega.Header).GetChunkInfo(chunk);
            Assert.Equal(chunk, info.Chunk);
            Assert.Equal(offset, info.Offset);
        }

        [Theory]
        [InlineData(0, 375)]
        [InlineData(168, 17)]
        [InlineData(169, 85)]
        [InlineData(170, 107)]
        [InlineData(41, 1100)]
        [InlineData(44, 1087)]
        [InlineData(138, 1031)]
        [InlineData(61, 541)]
        public void GettingGraphicsChunkInfoLenCheck(int chunk, int length)
        {
            var info = new EgaChunkHeader(Ega.Header).GetChunkInfo(chunk);
            Assert.Equal(chunk, info.Chunk);
            Assert.Equal(length, info.Length);
        }

        //GetChunkPath(string chunkName)
        [Theory]
        [InlineData(0, "PICTCNK")]
        [InlineData(21, "TITLCNK")]
        public void ReadGraphicsChunk(int chunk, string chunkName)
        {
            IChunkHeader header = new EgaChunkHeader(Ega.Header);
            var info = header.GetChunkInfo(chunk);
            var actualChunkBuffer = new byte[info.Length];

            using (var reader = new ChunkReader(File.Open(_egaGraphPath, FileMode.Open, FileAccess.Read, FileShare.Read), header))
            {
                reader.Read(info.Chunk, actualChunkBuffer, 0);
            }

            var expectedChunkBuffer = File.ReadAllBytes(GetSamplePath(chunkName));
            Assert.Equal(expectedChunkBuffer, actualChunkBuffer, EqualityComparer<byte>.Default);
        }



        [Theory]
        [InlineData(0, "PICTABLE")]
        [InlineData(21, "TITL")]
        public void HuffmanDecompression(int chunk, string decompChunkName)
        {
            IChunkHeader header = new EgaChunkHeader(Ega.Header);
            var info = header.GetChunkInfo(chunk);
            var chunkBuffer = new byte[info.Length];

            using (var reader = new ChunkReader(File.Open(_egaGraphPath, FileMode.Open, FileAccess.Read, FileShare.Read), header))
            {
                reader.Read(info.Chunk, chunkBuffer, 0);
            }

            IDecompressor decompressor = new HuffmanDecompressor(Ega.HuffmanDict);
            IChunkCompInfoGetter compInfoGetter = new EgaChunkCompInfoGetter();
            info = compInfoGetter.GetChunkInfo(chunk, chunkBuffer);

            var decompBuffer = new byte[info.Length];
            decompressor.Decompress(chunkBuffer, info.Offset, chunkBuffer.Length - info.Offset, decompBuffer, 0, decompBuffer.Length);

            var actualPicTable = decompBuffer;
            var expectedPicTable = File.ReadAllBytes(GetSamplePath(decompChunkName));
            Assert.Equal(actualPicTable, expectedPicTable, EqualityComparer<byte>.Default);
        }

        //EGA pallete
        //Value(pixels/flipped binary)
        //Blue 	 	0 	1 	0 	1 	0 	1 	0 	1 	0 	1 	0 	1 	0 	1 	0 	1
        //Green  	0 	0 	1 	1 	0 	0 	1 	1 	0 	0 	1 	1 	0 	0 	1 	1
        //Red 	 	0 	0 	0 	0 	1 	1 	1 	1 	0 	0 	0 	0 	1 	1 	1 	1
        //Intensity 0 	0 	0 	0 	0 	0 	0 	0 	1 	1 	1 	1 	1 	1 	1 	1
        //Result 	0 	1 	2 	3 	4 	5 	6 	7 	8 	9 	A   B   C   D   E   F
        [Theory]
        [InlineData(0, 0, 0, 0, 0)]
        [InlineData(1, 0, 0, 0, 1)]
        [InlineData(0, 1, 0, 0, 2)]
        [InlineData(1, 1, 0, 0, 3)]
        [InlineData(0, 0, 1, 0, 4)]
        [InlineData(1, 0, 1, 0, 5)]
        [InlineData(0, 1, 1, 0, 6)]
        [InlineData(1, 1, 1, 0, 7)]
        [InlineData(0, 0, 0, 1, 8)]
        [InlineData(1, 0, 0, 1, 9)]
        [InlineData(0, 1, 0, 1, 10)]
        [InlineData(1, 1, 0, 1, 11)]
        [InlineData(0, 0, 1, 1, 12)]
        [InlineData(1, 0, 1, 1, 13)]
        [InlineData(0, 1, 1, 1, 14)]
        [InlineData(1, 1, 1, 1, 15)]
        public void EgaPlanesColorConverter(int blue, int green, int red, int intensity, int color)
        {
            var actualColor = new EgaPlanesColorConverter().ToColor(blue, green, red, intensity);
            Assert.Equal(color, actualColor);
        }

        [Theory]
        [InlineData(0,  0x000000)]
        [InlineData(1,  0x0000AA)]
        [InlineData(2,  0x00AA00)]
        [InlineData(3,  0x00AAAA)]
        [InlineData(4,  0xAA0000)]
        [InlineData(5,  0xAA00AA)]
        [InlineData(6,  0xAA5500)]
        [InlineData(7,  0xAAAAAA)]
        [InlineData(8,  0x555555)]
        [InlineData(9,  0x5555FF)]
        [InlineData(10, 0x55FF55)]
        [InlineData(11, 0x55FFFF)]
        [InlineData(12, 0xFF5555)]
        [InlineData(13, 0xFF55FF)]
        [InlineData(14, 0xFFFF55)]
        [InlineData(15, 0xFFFFFF)]
        public void EgaToRgbColorConverter(int ega, int rgb)
        {
            var color = new EgaToRgbColorConverter().ToRgb(ega);
            Assert.Equal(rgb, color.ToArgb());
        }

        private PicTable[] ReadPicTable(byte[] buffer)
        {
            var picTable = new PicTable[GraphicConstants.NUMPICS];

            for (int i = 0; i < picTable.Length; i++)
            {
                picTable[i] = new PicTable(BitConverter.ToInt16(buffer, i*4), 
                    BitConverter.ToInt16(buffer, i*4 + 2));
            }

            return picTable;
        }

        private class PicTableEqualityComparer: EqualityComparer<PicTable>
        {
            public override bool Equals(PicTable x, PicTable y)
            {
                return x.Width == y.Width && x.Height == y.Height;
            }

            public override int GetHashCode(PicTable obj)
            {
                return obj.Width ^ obj.Height;
            }
        }

    }
}
