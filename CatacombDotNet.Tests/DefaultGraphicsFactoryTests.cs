﻿using CatacombDotNet.Core.Factories;
using CatacombDotNet.Implementation.Factories.Graphics;
using CatacombDotNet.Variables.Graphics;
using System;
using System.Configuration;
using System.IO;
using Xunit;

namespace CatacombDotNet.Tests
{
    public class DefaultGraphicsFactoryTests: IDisposable
    {
        private readonly IGraphicsFactory _factory;

        public DefaultGraphicsFactoryTests()
        {
            _factory = new DefaultGraphicsFactory(Path.GetFullPath(
                Path.Combine(Directory.GetCurrentDirectory(), 
                ConfigurationManager.AppSettings.Get("egaGraphPath"))));
        }

        [Fact]
        public void GettingText()
        {
            var text = _factory.GetText((int)Graphicnums.LEVEL1TEXT);
            Assert.Equal("\r\nPath to Keep\r\nKeep Entrance\r\nHidden Treasure Room\r\nHidden Gate Room\r\nBuried Healing Potions\r\nF\r\nG\r\nH\r\nI\r\nJ\r\nK\r\nL\r\nM\r\nN\r\nDescription O\r\nDescription P\r\nDescription Q\r\nDescription R\r\nDescription S\r\nDescription T\r\nDescription U\r\nDescription V\r\nDescription W\r\nDescription X\r\nDescription Y\r\nDescription Z\r\n\r\n", text);
        }

 
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_factory is IDisposable disposed)
                    {
                        disposed.Dispose();
                    }
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~DefaultGraphicsFactoryTests()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}
