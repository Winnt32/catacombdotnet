﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using CatacombDotNet.Core.Factories;
using CatacombDotNet.Implementation.Factories.Maps;
using CatacombDotNet.Variables.Maps;
using Xunit;

namespace CatacombDotNet.Tests
{
    public class DefaultMapFactoryTests: IDisposable
    {
        private readonly IMapFactory _factory;


        public DefaultMapFactoryTests()
        {
            _factory = new DefaultMapFactory(Path.GetFullPath(
                Path.Combine(Directory.GetCurrentDirectory(),
                    ConfigurationManager.AppSettings.Get("mapsPath"))));
        }

        [Fact]
        public void GettingMap()
        {
            var map = _factory.GetMap((int) Mapnames.APPROACH_MAP);
            var name = Encoding.ASCII.GetString(map.Name);
            var approach_map = map;
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_factory is IDisposable disposed)
                    {
                        disposed.Dispose();
                    }
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~DefaultGraphicsFactoryTests()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
