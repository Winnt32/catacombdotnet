﻿using CatacombDotNet.Core.Compression;
using CatacombDotNet.Core.Resources;
using CatacombDotNet.Core.Resources.Maps;
using CatacombDotNet.Implementation.Compression.Carmak;
using CatacombDotNet.Implementation.Compression.Rlew;
using CatacombDotNet.Implementation.Resources;
using CatacombDotNet.Implementation.Resources.Maps;
using CatacombDotNet.Variables.Maps;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Xunit;

namespace CatacombDotNet.Tests
{
    public class MapResourcesTests
    {
        private readonly string _mapsPath;

        public MapResourcesTests()
        {
            _mapsPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings.Get("mapsPath")));
        }

        private string GetSamplePath(string chunkName)
        {
            return Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), $@"Samples\{chunkName}"));
        }

        [Theory]
        [InlineData(0, "MAPCNK")]
        public void ReadMapChunk(int chunk, string sampleName)
        {
            IChunkHeader header = new MapChunkHeader(new MapFileType(Map.Header, 0, Map.Header.Length));
            var info = header.GetChunkInfo(chunk);
            var actualChunkBuffer = new byte[info.Length];

            using (var reader = new ChunkReader(File.Open(_mapsPath, FileMode.Open, FileAccess.Read, FileShare.Read), header))
            {
                reader.Read(info.Chunk, actualChunkBuffer, 0);
            }

            var expectedChunkBuffer = File.ReadAllBytes(GetSamplePath(sampleName));

            Assert.Equal(expectedChunkBuffer, actualChunkBuffer, EqualityComparer<byte>.Default);
        }

        [Theory]
        [InlineData(0, "MAPKP")]
        public void CarmackDecompressor(int chunk, string sampleName)
        {
            IDecompressor carmackDecompressor = new CarmackDecompressor();
            IChunkHeader header = new MapChunkHeader(new MapFileType(Map.Header, 0, Map.Header.Length));
            var info = header.GetChunkInfo(chunk);
            var chunkBuffer = new byte[info.Length];

            using (var reader = new ChunkReader(File.Open(_mapsPath, FileMode.Open, FileAccess.Read, FileShare.Read), header))
            {
                reader.Read(info.Chunk, chunkBuffer, 0);
            }

            var map = new MapType(chunkBuffer, 0, chunkBuffer.Length);
            
            for (var plane = 0; plane < MapConstants.MAPPLANES; plane++)
            {
                var pos = map.PlaneStart[plane];
                var compressed = map.PlaneLength[plane];

                if (compressed == 0)
                    continue;

                var planeBuffer = new byte[compressed];

                using (var stream = File.Open(_mapsPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    stream.Seek(pos, SeekOrigin.Begin);
                    stream.Read(planeBuffer, 0, planeBuffer.Length);
                }

                var expanded = BitConverter.ToUInt16(planeBuffer, 0);
                var actualExpandedBuffer = new byte[expanded];

                carmackDecompressor.Decompress(planeBuffer, 2, planeBuffer.Length - 2, actualExpandedBuffer, 0, actualExpandedBuffer.Length);

                var expectedExpandedBuffer = File.ReadAllBytes(GetSamplePath($"{sampleName}{plane}"));

                Assert.Equal(expectedExpandedBuffer, actualExpandedBuffer, EqualityComparer<byte>.Default);
            }
        }



        [Theory]
        [InlineData(0, "MAPRP")]
        public void RlewDecompressor(int chunk, string sampleName)
        {
            var mapFileType = new MapFileType(Map.Header, 0, Map.Header.Length);

            IDecompressor carmackDecompressor = new CarmackDecompressor();
            IDecompressor rlewDecompressor = new RlewDecompressor(mapFileType.RLEWtag);
            IChunkHeader header = new MapChunkHeader(mapFileType);

            var info = header.GetChunkInfo(chunk);
            var chunkBuffer = new byte[info.Length];

            using (var reader = new ChunkReader(File.Open(_mapsPath, FileMode.Open, FileAccess.Read, FileShare.Read), header))
            {
                reader.Read(info.Chunk, chunkBuffer, 0);
            }

            var map = new MapType(chunkBuffer, 0, chunkBuffer.Length);
            var size = map.Width * map.Height * 2;

            for (var plane = 0; plane < MapConstants.MAPPLANES; plane++)
            {
                var pos = map.PlaneStart[plane];
                var compressed = map.PlaneLength[plane];

                if (compressed == 0)
                    continue;

                var planeBuffer = new byte[compressed];

                using (var stream = File.Open(_mapsPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    stream.Seek(pos, SeekOrigin.Begin);
                    stream.Read(planeBuffer, 0, planeBuffer.Length);
                }

                var expanded = BitConverter.ToUInt16(planeBuffer, 0);
                var carmackBuffer = new byte[expanded];

                carmackDecompressor.Decompress(planeBuffer, 2, planeBuffer.Length - 2, carmackBuffer, 0, carmackBuffer.Length);

                var actualExpandedBuffer = new byte[size];

                rlewDecompressor.Decompress(carmackBuffer, 2, carmackBuffer.Length - 2, actualExpandedBuffer, 0, actualExpandedBuffer.Length);

                var expectedExpandedBuffer = File.ReadAllBytes(GetSamplePath($"{sampleName}{plane}"));

                Assert.Equal(expectedExpandedBuffer, actualExpandedBuffer, EqualityComparer<byte>.Default);
            }
        }

    }
}
