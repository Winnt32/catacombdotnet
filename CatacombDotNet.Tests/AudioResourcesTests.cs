﻿using CatacombDotNet.Core.Compression;
using CatacombDotNet.Core.Resources;
using CatacombDotNet.Implementation.Compression.Huffman;
using CatacombDotNet.Implementation.Resources;
using CatacombDotNet.Implementation.Resources.Sound;
using CatacombDotNet.Variables.Sound;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Xunit;

namespace CatacombDotNet.Tests
{
    public class AudioResourcesTests
    {
        private readonly string _audioPath;

        public AudioResourcesTests()
        {
            _audioPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings.Get("audioPath")));
        }

        private string GetSamplePath(string chunkName)
        {
            return Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), $@"Samples\{chunkName}"));
        }

        [Theory]
        [InlineData(30, "HITWALLC")]
        public void ReadAudioChunk(int chunk, string chunkName)
        {
            IChunkHeader header = new AudioChunkHeader(Audio.Header);
            var info = header.GetChunkInfo(chunk);
            var actualChunkBuffer = new byte[info.Length];

            using (var reader = new ChunkReader(File.Open(_audioPath, FileMode.Open, FileAccess.Read, FileShare.Read), header))
            {
                reader.Read(info.Chunk, actualChunkBuffer, 0);
            }

            var expectedChunkBuffer = File.ReadAllBytes(GetSamplePath(chunkName));
            Assert.Equal(expectedChunkBuffer, actualChunkBuffer, EqualityComparer<byte>.Default);
        }

        [Theory]
        [InlineData(30, "HITWALL")]
        public void HuffmanDecompression(int chunk, string decompChunkName)
        {
            IChunkHeader header = new AudioChunkHeader(Audio.Header);
            var info = header.GetChunkInfo(chunk);
            var chunkBuffer = new byte[info.Length];

            using (var reader = new ChunkReader(File.Open(_audioPath, FileMode.Open, FileAccess.Read, FileShare.Read), header))
            {
                reader.Read(info.Chunk, chunkBuffer, 0);
            }

            IDecompressor decompressor = new HuffmanDecompressor(Audio.HuffmanDict);
            IChunkCompInfoGetter compInfoGetter = new AudioChunkCompInfoGetter();
            info = compInfoGetter.GetChunkInfo(chunk, chunkBuffer);

            var decompBuffer = new byte[info.Length];
            decompressor.Decompress(chunkBuffer, info.Offset, chunkBuffer.Length - info.Offset, decompBuffer, 0, decompBuffer.Length);

            var expectedBuffer = File.ReadAllBytes(GetSamplePath(decompChunkName));
            Assert.Equal(decompBuffer, expectedBuffer, EqualityComparer<byte>.Default);
        }
    }
}