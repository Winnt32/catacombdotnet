﻿using System;
using System.IO;
using CatacombDotNet.Core.Compression;
using CatacombDotNet.Core.Factories;
using CatacombDotNet.Core.Resources;
using CatacombDotNet.Core.Resources.Sound;
using CatacombDotNet.Implementation.Compression.Huffman;
using CatacombDotNet.Implementation.Resources;
using CatacombDotNet.Implementation.Resources.Sound;
using CatacombDotNet.Variables.Sound;

namespace CatacombDotNet.Implementation.Factories.Sound
{
    public sealed class DefaultSoundFactory : ISoundFactory, IDisposable
    {
        public IChunkHeader ChunkHeader { get; private set; }
        public IChunkReader ChunkReader { get; private set; }
        public IDecompressor HuffmanDecompressor { get; private set; }
        public IChunkCompInfoGetter ChunkCompInfoGetter { get; private set; }

        private readonly byte[] _decBuffer; // decompress buffer

        private readonly byte[] _comBuffer; // compress buffer

        private readonly OplMusicWaveGenerator _oplMusicWaveGenerator = new OplMusicWaveGenerator(560);

        private readonly OplSoundWaveGenerator _oplSoundWaveGenerator = new OplSoundWaveGenerator(140);

        public DefaultSoundFactory(string audioPath)
        {
            if (!File.Exists(audioPath))
                throw new FileNotFoundException("AUDIO file not found!", audioPath);

            ChunkHeader = new AudioChunkHeader(CatacombDotNet.Variables.Sound.Audio.Header);
            ChunkReader = new ChunkReader(File.OpenRead(audioPath), ChunkHeader);
            HuffmanDecompressor = new HuffmanDecompressor(CatacombDotNet.Variables.Sound.Audio.HuffmanDict);
            ChunkCompInfoGetter = new AudioChunkCompInfoGetter();
            _decBuffer = new byte[64 * 1024]; // пока 64 Кб, необходимо провести аналитику 
            _comBuffer = new byte[64 * 1024]; // пока 64 Кб, необходимо провести аналитику 
        }

        // Только Adlib
        public MemoryStream GetSound(int chunk)
        {
            if (!(chunk >= SoundConstants.STARTADLIBSOUNDS && chunk < (SoundConstants.STARTADLIBSOUNDS + SoundConstants.NUMSOUNDS)))
                throw new ArgumentOutOfRangeException(nameof(chunk));

            var chunkInfo = ReadChunkIntoBuffer(chunk);
            var sound = new byte[chunkInfo.Length];
            Array.Copy(_decBuffer, 0, sound, 0, sound.Length);

            var soundEntry = new AdlibSoundEntry(sound, 0, sound.Length);

            var stream = new MemoryStream();
            _oplSoundWaveGenerator.GenerateWavSound(stream, soundEntry);

            return stream;
        }

        public MemoryStream GetMusic(int chunk)
        {
            if (chunk != SoundConstants.STARTMUSIC)
                throw new ArgumentOutOfRangeException(nameof(chunk));

            var chunkInfo = ReadChunkIntoBuffer(chunk);
            var music = new byte[chunkInfo.Length];
            Array.Copy(_decBuffer, 0, music, 0, music.Length);

            var musicSequence = new AdlibMusicSequence(music, 0, music.Length);

            var stream = new MemoryStream();
            _oplMusicWaveGenerator.GenerateWavMusic(stream, musicSequence);

            return stream;
        }

        private ChunkInfo ReadChunkIntoBuffer(int chunk)
        {
            var chunkComInfo = ChunkHeader.GetChunkInfo(chunk);
            ChunkReader.Read(chunkComInfo.Chunk, _comBuffer, 0);

            var chunkDecInfo = ChunkCompInfoGetter.GetChunkInfo(chunkComInfo.Chunk, _comBuffer);
            HuffmanDecompressor.Decompress(_comBuffer, chunkDecInfo.Offset, chunkComInfo.Length - chunkDecInfo.Offset, _decBuffer, 0, chunkDecInfo.Length);
            return chunkDecInfo;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {

                    if (ChunkReader is IDisposable disposable)
                        disposable.Dispose();

                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~DefaultSoundFactory()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
