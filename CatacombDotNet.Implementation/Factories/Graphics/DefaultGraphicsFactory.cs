﻿using System;
using System.Drawing;
using System.IO;
using System.Text;
using CatacombDotNet.Core.Compression;
using CatacombDotNet.Core.Factories;
using CatacombDotNet.Core.Resources;
using CatacombDotNet.Core.Resources.Graphics;
using CatacombDotNet.Implementation.Compression.Huffman;
using CatacombDotNet.Implementation.Resources;
using CatacombDotNet.Implementation.Resources.Graphics;
using CatacombDotNet.Variables.Graphics;

namespace CatacombDotNet.Implementation.Factories.Graphics
{
    public sealed class DefaultGraphicsFactory: IGraphicsFactory, IDisposable
    {
        private enum PicTableType
        {
            Pic,
            Picm,
            Sprite
        }

        public IChunkHeader ChunkHeader { get; private set; }
        public IChunkReader ChunkReader { get; private set; }
        public IDecompressor HuffmanDecompressor { get; private set; }
        public IPictureConverter PictureConverter { get; private set; }
        public IChunkCompInfoGetter ChunkCompInfoGetter { get; private set; }

        private readonly byte[] _decBuffer; // decompress buffer

        private readonly byte[] _comBuffer; // compress buffer

        private PicTable[] _picTable;

        private PicTable[] _picmTable;

        private SpriteTable[] _spriteTable;

        private readonly object[] _chunksCache;

        private readonly Bitmap[] _tile8Cache;

        public DefaultGraphicsFactory(string egaGraphPath)
        {
            if (!File.Exists(egaGraphPath))
                throw new FileNotFoundException("EGAGRAPH file not found!", egaGraphPath);

            ChunkHeader = new EgaChunkHeader(Ega.Header);
            ChunkReader = new ChunkReader(File.OpenRead(egaGraphPath), ChunkHeader);  
            HuffmanDecompressor = new HuffmanDecompressor(Ega.HuffmanDict);
            PictureConverter = new EgaPictureConverter(new EgaPlanesColorConverter(), new EgaToRgbColorConverter());
            ChunkCompInfoGetter = new EgaChunkCompInfoGetter();
            _decBuffer = new byte[64 * 1024]; // пока 64 Кб, необходимо провести аналитику 
            _comBuffer = new byte[64 * 1024]; // пока 64 Кб, необходимо провести аналитику 
            _chunksCache = new object[GraphicConstants.NUMCHUNKS];
            _tile8Cache = new Bitmap[GraphicConstants.NUMTILE8];
        }


        public Bitmap GetTile8x8(int tile)
        {
            if (tile < 0 && tile >= GraphicConstants.NUMTILE8)
                throw new ArgumentOutOfRangeException(nameof(tile));

            if (_chunksCache[GraphicConstants.STARTTILE8] == null)
            {
                var chunkInfo = ReadChunkIntoBuffer(GraphicConstants.STARTTILE8);
                var tile8Chunk = new byte[chunkInfo.Length];
                Array.Copy(_decBuffer, tile8Chunk, chunkInfo.Length);
                _chunksCache[GraphicConstants.STARTTILE8] = tile8Chunk;
            }


            var cacheTile = _tile8Cache[tile];

            if (cacheTile == null)
            {
                var bitmap = new Bitmap(8, 8); 
                PictureConverter.ToBitmap((byte[])_chunksCache[GraphicConstants.STARTTILE8], Ega.BLOCK*tile, Ega.BLOCK, bitmap);
                _tile8Cache[tile] = bitmap;
                cacheTile = bitmap;
            }

            return (Bitmap)cacheTile;
        }

        public MaskedBitmap GetSprite(int chunk)
        {
            if (chunk < GraphicConstants.STARTSPRITES || chunk >= (GraphicConstants.STARTSPRITES + GraphicConstants.NUMSPRITES))
                throw new ArgumentOutOfRangeException(nameof(chunk));

            var spriteIndex = chunk - GraphicConstants.STARTSPRITES;

            var cache = _chunksCache[chunk];

            if (cache != null)
                return (MaskedBitmap) cache;

            if (_spriteTable == null)
            {
                ReadChunkIntoBuffer(GraphicConstants.STRUCTSPRITE);
                _spriteTable = SpriteTable.FromByteArray(_decBuffer, GraphicConstants.NUMSPRITES);
            }

            var chunkInfo = ReadChunkIntoBuffer(chunk);
            var sprite = _spriteTable[spriteIndex];
            var maskedBitmap = new MaskedBitmap(new Bitmap(sprite.Width * 8, sprite.Height), new Bitmap(sprite.Width * 8, sprite.Height));
            PictureConverter.ToMaskedBitmap(_decBuffer, 0, chunkInfo.Length, maskedBitmap.Bitmap, maskedBitmap.Mask);

            _chunksCache[chunk] = maskedBitmap;

            return maskedBitmap;
        }

        /// <summary>
        /// Get pictures and 16x16 tiles
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns></returns>
        public Bitmap GetBitmap(int chunk)
        {
            Bitmap bitmap;
            ChunkInfo chunkInfo;

            var cache = _chunksCache[chunk];

            if (cache != null)
                return (Bitmap)cache;

            if (chunk >= GraphicConstants.STARTPICS && chunk < (GraphicConstants.STARTPICS + GraphicConstants.NUMPICS))
            {                
                var table = GetPicTable(PicTableType.Pic);
                var picIndex = chunk - GraphicConstants.STARTPICS;
                var pic = table[picIndex];
                chunkInfo = ReadChunkIntoBuffer(chunk);
                bitmap = new Bitmap(pic.Width * 8, pic.Height);
            }
            else if (chunk >= GraphicConstants.STARTTILE16 && chunk < (GraphicConstants.STARTTILE16 + GraphicConstants.NUMTILE16))
            {
                chunkInfo = ReadChunkIntoBuffer(chunk);
                bitmap = new Bitmap(16, 16);
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(chunk));
            }
         
            PictureConverter.ToBitmap(_decBuffer, 0, chunkInfo.Length, bitmap);

            _chunksCache[chunk] = bitmap;

            return bitmap;
        }

        /// <summary>
        /// Get masked pictures, masked 8x8 tiles and masked 16x16 tiles
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns></returns>
        public MaskedBitmap GetMaskedBitmap(int chunk)
        {
            MaskedBitmap maskedBitmap;
            ChunkInfo chunkInfo;

            var cache = _chunksCache[chunk];

            if (cache != null)
                return (MaskedBitmap)cache;

            if (chunk >= GraphicConstants.STARTPICM && chunk < (GraphicConstants.STARTPICM + GraphicConstants.NUMPICM))
            {
                var table = GetPicTable(PicTableType.Picm);
                var picmIndex = chunk - GraphicConstants.STARTPICM;
                var pic = table[picmIndex];
                chunkInfo = ReadChunkIntoBuffer(chunk);
                maskedBitmap = new MaskedBitmap(new Bitmap(pic.Width * 8, pic.Height), new Bitmap(pic.Width * 8, pic.Height));
            }
            else if (chunk >= GraphicConstants.STARTTILE8M && chunk < (GraphicConstants.STARTTILE8M + GraphicConstants.NUMTILE8M))
            {
                chunkInfo = ReadChunkIntoBuffer(chunk);
                maskedBitmap = new MaskedBitmap(new Bitmap(8, 8), new Bitmap(8, 8));
            }
            else if (chunk >= GraphicConstants.STARTTILE16M && chunk < (GraphicConstants.STARTTILE16M + GraphicConstants.NUMTILE16M))
            {
                chunkInfo = ReadChunkIntoBuffer(chunk);
                maskedBitmap = new MaskedBitmap(new Bitmap(16, 16), new Bitmap(16, 16));
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(chunk));
            }

            PictureConverter.ToMaskedBitmap(_decBuffer, 0, chunkInfo.Length, maskedBitmap.Bitmap, maskedBitmap.Mask);

            _chunksCache[chunk] = maskedBitmap;

            return maskedBitmap;
        }

        public string GetText(int chunk)
        {
            var cache = _chunksCache[chunk];

            if (cache != null)
                return (string) cache;

            if (!(chunk >= GraphicConstants.STARTEXTERNS && chunk < (GraphicConstants.STARTEXTERNS + GraphicConstants.NUMEXTERNS)))
                throw new ArgumentOutOfRangeException(nameof(chunk));

            var chunkInfo = ReadChunkIntoBuffer(chunk);
            var text = Encoding.ASCII.GetString(_decBuffer, 0, chunkInfo.Length);
            _chunksCache[chunk] = text;
            return text;
        }

        public Bitmap GetFont(int chunk, int charIndex)
        {
            if (!(chunk >= GraphicConstants.STARTFONT && chunk < (GraphicConstants.STARTFONT + GraphicConstants.NUMFONT)))
                throw new ArgumentOutOfRangeException(nameof(chunk));

            EgaFont font = null;

            var cache = _chunksCache[chunk];

            if (cache != null)
                font = (EgaFont)cache;

            if (font == null)
            {
                var chunkInfo = ReadChunkIntoBuffer(chunk);
                font = new EgaFont(_decBuffer, 0, chunkInfo.Length);
            }

            var fontChar = font[charIndex];
            var bitmap = new Bitmap(fontChar.Value.Width, fontChar.Value.Height);
            PictureConverter.ToCharBitmap(fontChar.Value.Data, 0, fontChar.Value.Data.Length, bitmap);
            return bitmap;
        }

        private PicTable[] GetPicTable(PicTableType picTableType)
        {
            PicTable[] table;
            int chunk;
            int num;

            switch (picTableType)
            {
                case PicTableType.Pic:
                    table = _picTable;
                    chunk = GraphicConstants.STRUCTPIC;
                    num = GraphicConstants.NUMPICS;
                    break;
                case PicTableType.Picm:
                    table = _picmTable;
                    chunk = GraphicConstants.STRUCTPICM;
                    num = GraphicConstants.NUMPICM;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(picTableType));
            }

            if (table != null)
                return table;

            ReadChunkIntoBuffer(chunk);

            table = PicTable.FromByteArray(_decBuffer, num);

            switch (picTableType)
            {
                case PicTableType.Pic:
                    _picTable = table;
                    break;
                case PicTableType.Picm:
                    _picmTable = table;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(picTableType));
            }

            return table;
        }

        private ChunkInfo ReadChunkIntoBuffer(int chunk)
        {
            var chunkComInfo = ChunkHeader.GetChunkInfo(chunk);
            ChunkReader.Read(chunkComInfo.Chunk, _comBuffer, 0);

            var chunkDecInfo = ChunkCompInfoGetter.GetChunkInfo(chunkComInfo.Chunk, _comBuffer);
            HuffmanDecompressor.Decompress(_comBuffer, chunkDecInfo.Offset, chunkComInfo.Length - chunkDecInfo.Offset, _decBuffer, 0, chunkDecInfo.Length);
            return chunkDecInfo;
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {

                    if (ChunkReader is IDisposable disposable)
                        disposable.Dispose();

                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~DefaultBitmapFactory() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion



    }
}
