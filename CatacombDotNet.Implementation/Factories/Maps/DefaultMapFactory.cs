﻿using System;
using System.IO;
using CatacombDotNet.Core.Factories;
using CatacombDotNet.Core.Resources;
using CatacombDotNet.Core.Resources.Maps;
using CatacombDotNet.Implementation.Resources;
using CatacombDotNet.Implementation.Resources.Maps;
using CatacombDotNet.Variables.Maps;

namespace CatacombDotNet.Implementation.Factories.Maps
{
    public sealed class DefaultMapFactory : IMapFactory, IDisposable
    {
        public IChunkHeader ChunkHeader { get; private set; }
        public IChunkReader ChunkReader { get; private set; }

        private readonly byte[] _buffer;

        public DefaultMapFactory(string mapsPath)
        {
            if (!File.Exists(mapsPath))
                throw new FileNotFoundException("GAMEMAPS file not found!", mapsPath);

            ChunkHeader = new MapChunkHeader(new MapFileType(Map.Header, 0, Map.Header.Length));
            ChunkReader = new ChunkReader(File.OpenRead(mapsPath), ChunkHeader);
            _buffer = new byte[MapType.SizeOf];
        }

        public MapType GetMap(int map)
        {
            if (map < (int) Mapnames.APPROACH_MAP || map > (int) Mapnames.NEMESISSLAIR_MAP)
                throw new ArgumentOutOfRangeException(nameof(map));

            ChunkReader.Read(map, _buffer, 0);
            return new MapType(_buffer, 0, _buffer.Length);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {

                    if (ChunkReader is IDisposable disposable)
                        disposable.Dispose();

                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~DefaultBitmapFactory() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
