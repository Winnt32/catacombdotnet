﻿using CatacombDotNet.Core.Resources;
using CatacombDotNet.Core.Resources.Maps;

namespace CatacombDotNet.Implementation.Resources.Maps
{
    public class MapChunkHeader : IChunkHeader
    {
        private readonly MapFileType _header;

        public MapChunkHeader(MapFileType header)
        {
            _header = header;
        }

        public ChunkInfo GetChunkInfo(int chunk)
        {
            return new ChunkInfo(chunk, _header.HeaderOffsets[chunk], MapType.SizeOf);
        }
    }
}
