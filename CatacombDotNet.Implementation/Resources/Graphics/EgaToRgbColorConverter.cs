﻿using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using CatacombDotNet.Core.Resources.Graphics;

namespace CatacombDotNet.Implementation.Resources.Graphics
{
    public sealed class EgaToRgbColorConverter: ICustomColorToRgbConverter
    {
        // http://www.shikadi.net/moddingwiki/EGA_Palette
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Color ToRgb(int color)
        {
            switch (color)
            {
                case 0:
                    return Color.FromArgb(0x000000);
                case 1:
                    return Color.FromArgb(0x0000AA);
                case 2:
                    return Color.FromArgb(0x00AA00);
                case 3:
                    return Color.FromArgb(0x00AAAA);
                case 4:
                    return Color.FromArgb(0xAA0000);
                case 5:
                    return Color.FromArgb(0xAA00AA);
                case 6:
                    return Color.FromArgb(0xAA5500);
                case 7:
                    return Color.FromArgb(0xAAAAAA);
                case 8:
                    return Color.FromArgb(0x555555);
                case 9:
                    return Color.FromArgb(0x5555FF);
                case 10:
                    return Color.FromArgb(0x55FF55);
                case 11:
                    return Color.FromArgb(0x55FFFF);
                case 12:
                    return Color.FromArgb(0xFF5555);
                case 13:
                    return Color.FromArgb(0xFF55FF);
                case 14:
                    return Color.FromArgb(0xFFFF55);
                case 15:
                    return Color.FromArgb(0xFFFFFF);
                default:
                    throw new ArgumentOutOfRangeException(nameof(color));
            }

        }
    }
}
