﻿using System;
using System.IO;
using System.Threading.Tasks;
using CatacombDotNet.Core.Resources;

namespace CatacombDotNet.Implementation.Resources
{
    //http://www.shikadi.net/moddingwiki/EGAGraph_Format
    public sealed class EgaChunkReader : IChunkReader, IDisposable
    {
        private readonly Stream _grFile;
        private readonly IChunkHeader _chunkHeader;

        public EgaChunkReader(Stream grFile, IChunkHeader chunkHeader) 
        {
            _grFile = grFile;
            _chunkHeader = chunkHeader;
        }

        /// <summary>
        /// ID_CA.C, line 1496: void CAL_ReadGrChunk (int chunk)
        /// </summary>
        /// <param name="chunk"></param>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public async ValueTask<int> ReadAsync(int chunk, byte[] buffer, int offset)
        {
            var chunkInfo = GetChunkInfoAndSetOffset(chunk, buffer, offset);
            return await _grFile.ReadAsync(buffer, offset, chunkInfo.Length);
        }

        /// <summary>
        /// ID_CA.C, line 1496: void CAL_ReadGrChunk (int chunk)
        /// </summary>
        /// <param name="chunk"></param>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public int Read(int chunk, byte[] buffer, int offset)
        {
            var chunkInfo = GetChunkInfoAndSetOffset(chunk, buffer, offset);
            return _grFile.Read(buffer, offset, chunkInfo.Length);
        }

        private ChunkInfo GetChunkInfoAndSetOffset(int chunk, byte[] buffer, int offset)
        {
            if (buffer == null)
                throw new ArgumentNullException(nameof(buffer));

            if (offset < 0)
                throw new ArgumentOutOfRangeException(nameof(offset));

            var chunkInfo = _chunkHeader.GetChunkInfo(chunk);

            if ((buffer.Length - offset) < chunkInfo.Length)
                throw new ArgumentOutOfRangeException(nameof(buffer.Length));

            _grFile.Seek(chunkInfo.Offset, SeekOrigin.Begin);

            return chunkInfo;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _grFile.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}
