﻿using System;
using CatacombDotNet.Core.Resources;

namespace CatacombDotNet.Implementation.Resources.Graphics
{
    public sealed class EgaChunkHeader : IChunkHeader
    {
        private readonly byte[] _header;

        public EgaChunkHeader(byte[] header)
        {
            _header = header;
        }

        // ID_CA.C line 1496, void CAL_ReadGrChunk (int chunk)
        public ChunkInfo GetChunkInfo(int chunk) // ID_CA.C line 2001, void CA_CacheMarks (char *title) ?
        {
            var offset = GetFilePosition(chunk);

            if (offset < 0)
                throw new ArgumentOutOfRangeException(nameof(chunk));

            int length;

            if (chunk <= 2) // void CAL_GetGrChunkLength(int chunk), ID_CA.C line 206
            {
                offset += 4;
                length = GetFilePosition(chunk + 1) - GetFilePosition(chunk) - 4;
            }
            else
            {
                var next = chunk + 1;

                while (GetFilePosition(next) == -1)
                    next++;

                length = GetFilePosition(next) - offset;
            }

            return new ChunkInfo(chunk, offset, length);
        }

        /// <summary>
        /// ID_CA.C, line 143: long GRFILEPOS(int c)
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns></returns>
        private int GetFilePosition(int chunk)
        {
            var value = BitConverter.ToInt32(_header, chunk * 3);

            value &= 0x00ffffff; //т.к. позиция графического chunk адресована 3-байтами

            if (value == 0x00ffffff)
                return -1;

            return value;
        }
    }
}
