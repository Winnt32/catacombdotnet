﻿using System.IO;
using CatacombDotNet.Audio.SampleProviders;

namespace CatacombDotNet.Implementation.Resources.Sound
{
    public class OplWaveStreamWriter
    {
        readonly BinaryWriter _writer;
        long _dataSizePos;
        int _dataChunkSize;
        IAudioSampleProvider _audioSampleProvider;

        public OplWaveStreamWriter(Stream stream)
        {
            _writer = new BinaryWriter(stream, System.Text.Encoding.UTF8);
        }

        public void SetSampleProvider(IAudioSampleProvider audioSampleProvider)
        {
            _audioSampleProvider = audioSampleProvider;

            _writer.Write(System.Text.Encoding.UTF8.GetBytes("RIFF"));
            _writer.Write(0); // placeholder
            _writer.Write(System.Text.Encoding.UTF8.GetBytes("WAVE"));

            _writer.Write(System.Text.Encoding.UTF8.GetBytes("fmt "));

            var extraSize = 0;
            var encoding = 1;
            var channels = audioSampleProvider.AudioFormat.Channels;
            var sampleRate = audioSampleProvider.AudioFormat.SampleRate;
            var bitsPerSample = audioSampleProvider.AudioFormat.BitsPerSample;
            var blockAlign = audioSampleProvider.AudioFormat.BlockAlign;
            var averageBytesPerSecond = audioSampleProvider.AudioFormat.AverageBytesPerSecond;
            _writer.Write((18 + extraSize)); // wave format length
            _writer.Write((short)encoding);
            _writer.Write((short)channels);
            _writer.Write(sampleRate);
            _writer.Write(averageBytesPerSecond);
            _writer.Write((short)blockAlign);
            _writer.Write((short)bitsPerSample);
            _writer.Write((short)extraSize);

            _writer.Write(System.Text.Encoding.UTF8.GetBytes("data"));
            _dataSizePos = _writer.BaseStream.Position;
            _writer.Write(0); // placeholder
        }

        public void Write(byte[] data, int dataLength)
        {
            EndStream();
            _writer.Write(data, 0, dataLength);
            _dataChunkSize += dataLength;
            UpdateHeader();
        }

        void UpdateHeader()
        {
            UpdateRiffChunk();
            UpdateDataChunk();
        }

        void UpdateDataChunk()
        {
            _writer.Seek((int)_dataSizePos, SeekOrigin.Begin);
            _writer.Write((uint)_dataChunkSize);
        }

        void UpdateRiffChunk()
        {
            _writer.Seek(4, SeekOrigin.Begin);
            _writer.Write((uint)(_writer.BaseStream.Length - 8));
        }

        void EndStream()
        {
            _writer.Seek(0, SeekOrigin.End);
        }
    }
}
