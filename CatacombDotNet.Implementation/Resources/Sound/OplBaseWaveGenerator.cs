﻿using System;
using System.IO;
using CatacombDotNet.Audio;
using CatacombDotNet.Audio.OPL;
using CatacombDotNet.Audio.OPL.DosBox;
using CatacombDotNet.Audio.SampleProviders;

namespace CatacombDotNet.Implementation.Resources.Sound
{
    public abstract class OplBaseWaveGenerator: AudioSampleProvider16
    {
        public int Rate { get; }

        public int TicksPerSecond { get; }

        public int SamplesPerTick { get; }

        public override AudioFormat AudioFormat { get; }

        protected readonly IOpl Opl;

        protected OplBaseWaveGenerator(int rate, int ticksPerSecond)
        {
            Rate = rate;
            TicksPerSecond = ticksPerSecond;
            SamplesPerTick = rate / TicksPerSecond;
            AudioFormat = new AudioFormat(rate, channels: 1, bits: 16);
            Opl = new DosBoxOPL(OplType.Opl2);
            Opl.Init(Rate);
            Init();
        }

        protected OplBaseWaveGenerator(int ticksPerSecond) : this(44100, ticksPerSecond)
        {

        }

        protected void Init()
        {
            Opl.WriteReg(1, 0x20);
            Opl.WriteReg(8, 0);
        }

        protected void Clean()
        {
            const int alEffects = 0xbd;
            Opl.WriteReg(alEffects, 0);
            Opl.WriteReg(1, 0);
            for (var i = 5; i < 0xf5; i++)
            {
                Opl.WriteReg(i, 0);
            }
        }
        
        public override int Read(short[] samples, int count)
        {
            Opl.ReadBuffer(samples, 0, count);
            return count;
        }

        protected OplWaveStreamWriter CreateWaveSteamWriter(Stream stream)
        {
            var streamWriter = new OplWaveStreamWriter(stream);
            streamWriter.SetSampleProvider(this);
            return streamWriter;
        }
    }
}
