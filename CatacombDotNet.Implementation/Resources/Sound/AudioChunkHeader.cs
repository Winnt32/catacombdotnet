﻿using CatacombDotNet.Core.Resources;
using System;
using System.Runtime.CompilerServices;

namespace CatacombDotNet.Implementation.Resources.Sound
{
    /// <summary>
    /// ID_CA.C, line 1065: void CA_CacheAudioChunk (int chunk)
    /// </summary>
    public sealed class AudioChunkHeader : IChunkHeader
    {
        private readonly byte[] _header;

        public AudioChunkHeader(byte[] header)
        {
            _header = header;
        }

        public ChunkInfo GetChunkInfo(int chunk)
        {
            var offset = GetFilePosition(chunk);

            if (offset < 0)
                throw new ArgumentOutOfRangeException(nameof(chunk));

            var length = GetFilePosition(chunk + 1) - offset;

            return new ChunkInfo(chunk, offset, length);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetFilePosition(int chunk)
        {
            return BitConverter.ToInt32(_header, chunk * 4);
        }
    }
}
