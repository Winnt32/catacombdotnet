﻿using CatacombDotNet.Core.Resources;
using System;

namespace CatacombDotNet.Implementation.Resources.Sound
{
    public sealed class AudioChunkCompInfoGetter : IChunkCompInfoGetter
    {
        public ChunkInfo GetChunkInfo(int chunk, byte[] compChunk)
        {
            return new ChunkInfo(chunk, 4, BitConverter.ToInt32(compChunk, 0));
        }
    }
}
