﻿using CatacombDotNet.Core.Compression;

namespace CatacombDotNet.Implementation.Compression.Huffman
{
    public sealed class HuffmanDecompressor: IDecompressor
    {
        private readonly byte[] _huffmanDict;

        public HuffmanDecompressor(byte[] huffmanDict)
        {
            _huffmanDict = huffmanDict;
        }

        //ID_CA.C CAL_HuffExpand line -> 396
        //Механизм расжимания через бинарное дерево хаффмана
        //Каждая нода дерева группой из 4-х байт -> 
        //например: первая подгруппа из 2-х байт - левая ветвь, вторая подгруппа из 2-x байт - правая ветвь.
        //первый байт в подгруппе обычно битовый (0 или 1) обзначающий что содержит ветвь -
        //значение (обычно 0) или ссылку на следующую ветвь (обычно 1)
        //написано, что у ID биты в байте лежат в обратном порядке http://www.shikadi.net/moddingwiki/Huffman_Compression

        //1. Выставляем указатель huffnode *headptr на root ноду 254
        //2.1 загружаем значение headptr в регистр bx
        //2.2 загружаем значения: sourceoff в si, destoff в di, destseg в es, sourceseg в ds, endoff в ax
        //2.3 загружаем первый байт source в сh
        //2.4 выполняем инкремент si (sourceoff)
        //2.5 загружаем 1 в сl (для проверки первого байта подгруппы)
        //2.6 коммандной test выполняем побитовое сравнение значения ch и cl (т.е является ли подгруппа ветвью)
        //2.7 переход jnz -> bit1short когда ZF = 0 (флаг нуля)
        //3 если первый бит в source = 1
        //3.1 mov dx, [ss:bx+2] -> переставляем указатель huffnode *headptr на bit1
        //3.2 shl сдвигаем cl на 1 разряд влево (выдвигаемый бит становится значением флага переноса)
        //3.3 переход jnc -> sourceupshort когда CF = 0 (флаг переноса)
        //4.1 or dh, dh -> если ZF = 1 (dx < 256) то ветвь содержит значение байта

        //http://www.codenet.ru/progr/asm/tasm/39.php
        //http://av-assembler.ru/asm/afd/asm-flags-register.htm
        //http://av-assembler.ru/instructions/test.php

        //typedef struct { unsigned short bit0, bit1; } huffnode;

        //254 node
        //0x00 0x00 0xFD 0x01
        //
        //0000 0010->загружаем первый байт из source(ch)
        //0000 0001 -> (cl)
        //
        //TEST
        //
        //0000 0000->ZF = 1
        //
        //
        //0000 0001->загружаем первый байт из source(ch)
        //0000 0001 -> (cl)
        //
        //0000 0001->ZF = 0

        //00000000 -> 0x0
        //00000001 -> 0x1
        //00000010 -> 0x2
        //00000100 -> 0x4
        //00001000 -> 0x8
        //00010000 -> 0x10
        //00100000 -> 0x20
        //01000000 -> 0x40
        //10000000 -> 0x80

        //Организовать проверку алгоритма на Picture table, Masked Picture Table, Sprite table (ID_CA.C line 811 void CAL_SetupGrFile (void))
        public unsafe void Decompress(byte[] srcBuffer, int srcOffset, int srcCount, byte[] trgBuffer, int trgOffset, int trgCount)
        {
            fixed (byte* huffmanDict = _huffmanDict)
            fixed (byte* src = srcBuffer)
            fixed (byte* trg = trgBuffer)
            {

                HuffmanNode* huffmanTable = (HuffmanNode*)huffmanDict;
                HuffmanNode* headptr = huffmanTable + 254;

                byte* source = src + srcOffset;
                byte* target = trg + trgOffset;

                byte bit1_mask = 1;
                int count = 0;

                while (count != trgCount)
                {
                    ushort leaf;

                    if (((*source) & bit1_mask) == bit1_mask)
                        leaf = headptr->Bit1;
                    else
                        leaf = headptr->Bit0;

                    if (leaf < 256)
                    {
                        *target = (byte) leaf;
                        target++;
                        count++;
                        headptr = huffmanTable + 254;
                    }
                    else
                    {

                        if (((*source) & bit1_mask) == bit1_mask)
                            headptr = huffmanTable + ((headptr->Bit1) - 256);
                        else
                            headptr = huffmanTable + ((headptr->Bit0) - 256);
                    }

                    bit1_mask = (byte) (bit1_mask << 1);

                    if (bit1_mask == 0)
                    {
                        source++;
                        bit1_mask = 1;
                    }
                }
            }
        }
    }
}
