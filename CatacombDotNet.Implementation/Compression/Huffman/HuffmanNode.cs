﻿using System.Runtime.InteropServices;

namespace CatacombDotNet.Implementation.Compression.Huffman
{
    [StructLayout(LayoutKind.Sequential)]
    internal struct HuffmanNode
    {
        public ushort Bit0;
        public ushort Bit1;
    }
}
