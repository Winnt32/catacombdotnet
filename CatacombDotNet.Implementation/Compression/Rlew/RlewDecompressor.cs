﻿using CatacombDotNet.Core.Compression;

namespace CatacombDotNet.Implementation.Compression.Rlew
{
    public class RlewDecompressor: IDecompressor
    {
        private readonly ushort _RLEWtag;

        public RlewDecompressor(ushort RLEWtag)
        {
            _RLEWtag = RLEWtag;
        }

        public unsafe void Decompress(byte[] srcBuffer, int srcOffset, int srcCount, byte[] trgBuffer, int trgOffset, int trgCount)
        {
            fixed (void* sourceV = &srcBuffer[srcOffset], destV = &trgBuffer[trgOffset])
            {
                var source = (ushort*)sourceV;
                var dest = (ushort*)destV;
                var end = dest + trgCount / 2;

                do
                {
                    var value = *source;
                    ++source;

                    if (value != _RLEWtag)
                    {
                        *dest = value;
                        ++dest;
                    }
                    else
                    {
                        var count = *source;
                        ++source;

                        value = *source;
                        ++source;

                        for (var i = 1; i <= count; i++)
                        {
                            *dest = value;
                            ++dest;
                        }
                    }
                } while (dest < end);
            }
        }
    }
}
