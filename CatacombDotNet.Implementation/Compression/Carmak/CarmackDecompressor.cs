﻿using CatacombDotNet.Core.Compression;

namespace CatacombDotNet.Implementation.Compression.Carmak
{
    public sealed class CarmackDecompressor: IDecompressor
    {
        private const byte NEARTAG = 0xa7;
        private const byte FARTAG = 0xa8;

        public unsafe void Decompress(byte[] srcBuffer, int srcOffset, int srcCount, byte[] trgBuffer, int trgOffset, int trgCount)
        {
            fixed (void* inptrV = &srcBuffer[srcOffset], outptrV = &trgBuffer[trgOffset])
            {
                var length = trgCount / 2;

                ushort  ch, chhigh, count, offset;
                ushort* copyptr, inptr, outptr, dest;

                inptr = (ushort*) inptrV;
                outptr = (ushort*) outptrV;
                dest = (ushort*) outptrV;

                while (length > 0)
                {
                    ch = *inptr++;
                    chhigh = (ushort) (ch >> 8);
                    if (chhigh == NEARTAG)
                    {
                        count = (ushort) (ch & 0xff);
                        if (count == 0)
                        {               // have to insert a word containing the tag byte
                            var inptrB = (byte*) inptr;
                            ch |= *inptrB++;
                            inptr = (ushort*) inptrB;
                            *outptr++ = ch;
                            length--;
                        }
                        else
                        {
                            var inptrB = (byte*)inptr;
                            offset = *inptrB++;
                            inptr = (ushort*)inptrB;
                            copyptr = outptr - offset;
                            length -= count;
                            while (count-- > 0)
                                *outptr++ = *copyptr++;
                        }
                    }
                    else if (chhigh == FARTAG)
                    {
                        count = (ushort) (ch & 0xff);
                        if (count == 0)
                        {               // have to insert a word containing the tag byte
                            var inptrB = (byte*)inptr;
                            ch |= *inptrB++;
                            inptr = (ushort*)inptrB;
                            *outptr++ = ch;
                            length--;
                        }
                        else
                        {
                            offset = *inptr++;
                            copyptr = dest + offset;
                            length -= count;
                            while (count-- > 0)
                                *outptr++ = *copyptr++;
                        }
                    }
                    else
                    {
                        *outptr++ = ch;
                        length--;
                    }
                }
            }
        }
    }
}
