﻿using CatacombDotNet.Implementation.Factories.Graphics;
using CatacombDotNet.Variables.Graphics;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.DXGI;
using System;
using System.Drawing.Imaging;

namespace CatacombDotNet.Engine
{
    public sealed class GameEngine: IDisposable
    {
        private readonly DefaultGraphicsFactory _gFactory;
        private readonly RenderTarget _renderTarget;

        private Bitmap _bitmapsCache;

        private const int START_BITMAP = GraphicConstants.STARTPICS;
        private const int END_BITMAP = GraphicConstants.STARTPICS + GraphicConstants.NUMPICS - 1;

        private int _currentBitmap;

        public GameEngine(DefaultGraphicsFactory gFactory, RenderTarget renderTarget)
        {
            _gFactory = gFactory;
            _renderTarget = renderTarget;
            _currentBitmap = START_BITMAP;
            //_bitmapsCache = new Bitmap[GraphicConstants.NUMPICS];
            LoadFromBitmap(_gFactory.GetBitmap(_currentBitmap));
        }

        public void Next()
        {
            if (_currentBitmap < END_BITMAP)
                _currentBitmap++;
            else
                _currentBitmap = START_BITMAP;

            //if (_bitmapsCache[_currentBitmap - GraphicConstants.STARTPICS] == null)
            //{
                LoadFromBitmap(_gFactory.GetBitmap(_currentBitmap));
            //}                
        }

        public void Back()
        {
            if (_currentBitmap > START_BITMAP)
                _currentBitmap--;
            else
                _currentBitmap = END_BITMAP;

            //if (_bitmapsCache[_currentBitmap - GraphicConstants.STARTPICS] == null)
            //{
                LoadFromBitmap(_gFactory.GetBitmap(_currentBitmap));
            //}
        }

        private void LoadFromBitmap(System.Drawing.Bitmap bitmap)
        {
            var sourceArea = new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height);
            var bitmapProperties = new BitmapProperties(new SharpDX.Direct2D1.PixelFormat(Format.B8G8R8A8_UNorm, SharpDX.Direct2D1.AlphaMode.Ignore));
            var bitmapData = bitmap.LockBits(sourceArea, ImageLockMode.ReadOnly, bitmap.PixelFormat);
            _bitmapsCache = new Bitmap(_renderTarget, new Size2(bitmap.Width, bitmap.Height), new DataPointer(bitmapData.Scan0, 4), bitmapData.Stride, bitmapProperties);
            bitmap.UnlockBits(bitmapData);
        }


        public void Draw()
        {

            ////https://docs.microsoft.com/en-us/windows/desktop/direct2d/how-to-clip-with-bitmap-masks
            //_renderTarget2D.AntialiasMode = AntialiasMode.Aliased;
            //_renderTarget2D.FillOpacityMask(_mask, _bb, OpacityMaskContent.Graphics);
            //_renderTarget2D.AntialiasMode = AntialiasMode.PerPrimitive;

            _renderTarget.DrawBitmap(_bitmapsCache, 1.0f, BitmapInterpolationMode.Linear);
        }

        // Для MaskedBitmap
        //    _bitmap = LoadFromBitmap(_renderTarget2D, mBitmap.Bitmap);
        //    _bb = new BitmapBrush(_renderTarget2D, _bitmap,
        //            new BitmapBrushProperties
        //            {
        //                ExtendModeX = ExtendMode.Clamp,
        //                ExtendModeY = ExtendMode.Clamp,
        //                InterpolationMode = BitmapInterpolationMode.NearestNeighbor
        //});
        //        _mask = LoadFromBitmap(_renderTarget2D, mBitmap.Mask);
        
    #region IDisposable Support
    private bool disposedValue = false; // To detect redundant calls

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _gFactory.Dispose();
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~GameEngine()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
