                           About the game                         


     In CATACOMB 3-D you are the high wizard who must rescue your friend 
     from the clutches of the evil lich Nemesis.  Using your magical 
     powers, you venture forth into the bizzare dimensions of the 
     Catacombs.

                        Getting started                           


Starting this game is as simple as typing the following at the DOS prompt:


START followed by the [ENTER] key.


If you need further help, call our Technical Support group at 1-318-221-8311.


                      Summary of how to play                      


After the title screen appears, you can press the SPACE key to bring up 
the Control Panel.  From the Control Panel you can do any of the following
actions:
        
        Start a New Game
        Load a Saved Game
        Save the current game in progress
        Configure your system
        Return to current game in progress
        End current game in progress
        Play Paddle Wars (just a minor diversion)
        Quit altogether

These options are fairly self-explanatory once you try them out.

Playing the game

In CATACOMB 3-D you must explore deeper and deeper into Nemesis's 
domain within the Catacombs.  Here are the things you can do:

 WALK.  Use the arrow keys, joystick, or mouse to walk around.

 RUN.  Hold down the SHIFT key when using the keyboard controls and you 
  will walk/run faster.  In joystick mode, push the joystick all the way 
  forward.  Mouse users just move the mouse faster.

 FIRE A MAGIC BOLT.  Hold down the CTRL key (or joystick and mouse 
  Button 1) to build shot power, then release the button to unleash a 
  deadly magic bolt.  If you let it build up completely, a huge bolt will 
  be released.

 STRAFE.  Hold down the ALT key (or Button 2) to run sideways.  This is 
  very helpful when a large batch of monsters is coming toward you.  You 
  can move side-to-side while firing repeatedly and mow them down.

 GET ITEM.  To get items off the floor, simply move over them.  You will 
  find powerspheres, chests, keys, scrolls, and healing potions.

 HEAL.  Press H or SPACEBAR to drink a healing potion.

 NUKE.  If you picked up a Nuke Powersphere, press N or ENTER to release 
  a magic burst in all directions.

 BOLT.  If you picked up a Bolt Powersphere, press B to unleash a stream 
  of magic bolts at an opponent.

 READ SCROLL.  To read a scroll, press the corresponding number key at the 
  top of our keyboard.

 EXIT LEVEL.  To get to the next level of the castle, find the magic gate.
  It will look like a pulsating blue cloud.

 OPEN DOOR.  To open a magic door, you'll need a key of the same color as
  the energy barring the way.

 EXPLODE A WALL.  There are many hidden chambers behind the walls of the 
  hallways you will traverse.  Systematically spray the walls with magic 
  bolts in order to uncover the explodable walls that will disappear 
  revealing the chambers beyond.


Play strategies


  Don't run through the halls blindly.  You will get into fatal trouble
   for sure that way.

  Use the side-stepping strafing technique to peek around corners. 

  Dispose of monsters while they are still far away from you.  When they
   get close, they will hurt you.
