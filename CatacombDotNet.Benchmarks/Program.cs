﻿using BenchmarkDotNet.Running;

namespace CatacombDotNet.Benchmarks
{
    class Program
    {
        static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run<EgaPictureConverterBenchmark>();
        }
    }
}
