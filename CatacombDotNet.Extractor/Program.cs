﻿using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using CatacombDotNet.Core.Compression;
using CatacombDotNet.Core.Resources;
using CatacombDotNet.Core.Resources.Graphics;
using CatacombDotNet.Implementation.Compression.Huffman;
using CatacombDotNet.Implementation.Resources;
using CatacombDotNet.Implementation.Resources.Graphics;
using CatacombDotNet.Variables.Graphics;

namespace CatacombDotNet.Extractor
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Length == 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(args),
                        @"Please enter a numpic argument. Usage: CatacombDotNet.Extractor <numpic> <numpic> ...");
                }

                var grFiles = new Graphicnums[args.Length];

                for (var i = 0; i < args.Length; i++)
                {
                    if (Enum.TryParse<Graphicnums>(args[i], true, out var grFile))
                    {
                        if ( (int)grFile < GraphicConstants.STARTPICS || (int)grFile > GraphicConstants.STARTPICM)
                            throw new ArgumentOutOfRangeException(nameof(grFile), @"Numpic out of range");

                        grFiles[i] = grFile;
                    }
                    else
                    {
                        throw new ArgumentException($@"Graphicnum {args[i]} not found!");
                    }
                }

                var cDir = Directory.GetCurrentDirectory();

                var egaGraphPath = Path.GetFullPath(Path.Combine(cDir,
                    ConfigurationManager.AppSettings.Get("egaGraphPath")));

                if (!File.Exists(egaGraphPath))
                {
                    throw new FileNotFoundException(@"EGAGRAPH file not found!");
                }

                var extrPath = Path.GetFullPath(Path.Combine(cDir,
                    ConfigurationManager.AppSettings.Get("extrPath")));

                if (!Directory.Exists(extrPath))
                    Directory.CreateDirectory(extrPath);

                IChunkHeader header = new EgaChunkHeader(Ega.Header);
                IChunkCompInfoGetter compInfoGetter = new EgaChunkCompInfoGetter();
                IDecompressor decompressor = new HuffmanDecompressor(Ega.HuffmanDict);

                //считываем picture table
                var chunkPtInfo = header.GetChunkInfo(GraphicConstants.STRUCTPIC); 
                var chunkPtBuffer = new byte[chunkPtInfo.Length];

                using (var reader = new ChunkReader(new FileStream(egaGraphPath, FileMode.Open,
                    FileAccess.Read, FileShare.Read), header))
                {
                    reader.Read(chunkPtInfo.Chunk, chunkPtBuffer, 0); 
                }

                chunkPtInfo = compInfoGetter.GetChunkInfo(chunkPtInfo.Chunk, chunkPtBuffer);

                var decompChunkPtBuffer = new byte[chunkPtInfo.Length];
                decompressor.Decompress(chunkPtBuffer, chunkPtInfo.Offset, chunkPtBuffer.Length - chunkPtInfo.Offset,
                    decompChunkPtBuffer, 0, decompChunkPtBuffer.Length);

                var picTable = PicTable.FromByteArray(decompChunkPtBuffer, GraphicConstants.NUMPICS);

                Parallel.ForEach(grFiles, new ParallelOptions() { MaxDegreeOfParallelism = Environment.ProcessorCount }, grFile =>
                {
                    var chunk = (int)grFile;

                    var chunkInfo = header.GetChunkInfo(chunk); // информация о запакованном chunk
                    var chunkBuffer = new byte[chunkInfo.Length];

                    using (var reader = new ChunkReader(new FileStream(egaGraphPath, FileMode.Open,
                        FileAccess.Read, FileShare.Read), header))
                    {
                        reader.Read(chunkInfo.Chunk, chunkBuffer, 0); // чтение запакованного chunk
                    }

                    chunkInfo = compInfoGetter.GetChunkInfo(chunk, chunkBuffer); // информация о распакованном chunk

                    var decompChunkBuffer = new byte[chunkInfo.Length];
                    decompressor.Decompress(chunkBuffer, chunkInfo.Offset, chunkBuffer.Length - chunkInfo.Offset,
                        decompChunkBuffer, 0, decompChunkBuffer.Length); // распаковка chunk

                    var egaPicConverter = new EgaPictureConverter(new EgaPlanesColorConverter(), new EgaToRgbColorConverter());

                    var pic = picTable[chunk - GraphicConstants.STARTPICS];

                    using (var bitmap = new Bitmap(pic.Width * 8, pic.Height)) // конвертация в Bitmap
                    {
                        egaPicConverter.ToBitmap(decompChunkBuffer, 0, decompChunkBuffer.Length, bitmap);
                        bitmap.Save(Path.Combine(extrPath, $@"{Enum.GetName(typeof(Graphicnums), chunk)}.bmp"), ImageFormat.Bmp);
                    }
       
                    Console.WriteLine($@"Picture {Enum.GetName(typeof(Graphicnums), chunk)} successfully extracted.");

                });
               
                Console.WriteLine(@"All pictures successfully extracted.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
