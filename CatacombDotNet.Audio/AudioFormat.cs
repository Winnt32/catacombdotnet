﻿using System;

namespace CatacombDotNet.Audio
{
    public struct AudioFormat
    {
        readonly int _channels;
        readonly int _sampleRate;
        readonly int _bitsPerSample;
        readonly int _blockAlign;
        readonly int _averageBytesPerSecond;

        /// <summary>
        /// Gets the number of channels.
        /// </summary>
        public int Channels
        {
            get
            {
                return _channels;
            }
        }

        /// <summary>
        /// Gets the sample rate.
        /// </summary>
        /// <description>>Number of samples per second.</description>
        public int SampleRate
        {
            get
            {
                return _sampleRate;
            }
        }

        /// <summary>
        /// Gets the average number of bytes used per second.
        /// </summary>
        public int AverageBytesPerSecond
        {
            get
            {
                return _averageBytesPerSecond;
            }
        }

        /// <summary>
        /// Gets the block alignment.
        /// </summary>
        public int BlockAlign
        {
            get
            {
                return _blockAlign;
            }
        }

        /// <summary>
        /// Gets the number of bits per sample.
        /// </summary>
        public int BitsPerSample
        {
            get
            {
                return _bitsPerSample;
            }
        }

        public AudioFormat(int rate = 44100, int channels = 2, int bits = 16)
        {
            if (channels < 1)
            {
                throw new ArgumentOutOfRangeException("channels", "Channels must be 1 or greater");
            }
            _channels = channels;
            _sampleRate = rate;
            _bitsPerSample = bits;

            _blockAlign = channels * (bits / 8);
            _averageBytesPerSecond = _sampleRate * _blockAlign;
        }
    }
}
