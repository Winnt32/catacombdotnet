namespace CatacombDotNet.Audio.OPL
{
    public enum OplType
    {
        Opl2,
        DualOpl2,
        Opl3
    }

    public interface IOpl
    {
        void Init(int rate);

        void WriteReg(int r, int v);

        void ReadBuffer(short[] buffer, int pos, int length);

        bool IsStereo { get; }
    }
}