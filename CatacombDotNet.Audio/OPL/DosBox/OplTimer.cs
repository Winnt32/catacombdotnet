#define DBOPL_WAVE_EQUALS_WAVE_TABLEMUL

using System;

namespace CatacombDotNet.Audio.OPL.DosBox
{
    class OplTimer
    {
        const double Epsilon = 0.1;

        public double StartTime { get; private set; }

        public double Delay { get; private set; }

        public bool Enabled { get; private set; }

        public bool Overflow { get; set; }

        public bool Masked { get; set; }

        public byte Counter { get; set; }

        //Call update before making any further changes
        public void Update(double time)
        {
            if (!Enabled || Math.Abs(Delay) < Epsilon)
                return;
            double deltaStart = time - StartTime;
            // Only set the overflow flag when not masked
            if (deltaStart >= 0 && !Masked)
                Overflow = true;
        }

        //On a reset make sure the start is in sync with the next cycle
        public void Reset(double time)
        {
            Overflow = false;
            if (Math.Abs(Delay) < Epsilon || !Enabled)
                return;
            double delta = (time - StartTime);
//            double rem = fmod(delta, delay);
            double rem = delta % Delay;
            double next = Delay - rem;
            StartTime = time + next;
        }

        public void Stop()
        {
            Enabled = false;
        }

        public void Start(double time, int scale)
        {
            //Don't enable again
            if (Enabled)
                return;
            Enabled = true;
            Delay = 0.001 * (256 - Counter) * scale;
            StartTime = time + Delay;
        }
    }
    
}