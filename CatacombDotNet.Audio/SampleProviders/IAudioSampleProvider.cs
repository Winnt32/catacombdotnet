namespace CatacombDotNet.Audio.SampleProviders
{
    public interface IAudioSampleProvider
    {
        AudioFormat AudioFormat { get; }

        int Read(byte[] samples, int count);
    }
}