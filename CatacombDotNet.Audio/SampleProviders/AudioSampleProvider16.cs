namespace CatacombDotNet.Audio.SampleProviders
{
    public abstract class AudioSampleProvider16 : IAudioSampleProvider
    {
        public abstract AudioFormat AudioFormat
        {
            get;
        }

        public abstract int Read(short[] samples, int count);

        public int Read(byte[] samples, int count)
        {
            var buffer = new short[count / 2];
            var numRead = Read(buffer, count / 2);
            var offs = 0;
            for (int i = 0; i < numRead; i++)
            {
                samples[offs++] = (byte)(buffer[i] & 0xFF);
                samples[offs++] = (byte)((buffer[i] >> 8) & 0xFF);
            }
            return numRead * 2;
        }
    }
}