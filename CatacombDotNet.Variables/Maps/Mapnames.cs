﻿namespace CatacombDotNet.Variables.Maps
{
    /// <summary>
    /// MAPSC3D.H
    /// </summary>
    public enum Mapnames
    {
        APPROACH_MAP,            // 0
        ENTRANCE_MAP,            // 1
        GROUND_FLOOR_MAP,        // 2
        SECOND_FLOOR_MAP,        // 3
        THIRD_FLOOR_MAP,         // 4
        TOWER_1_MAP,             // 5
        TOWER_2_MAP,             // 6
        SECRET_HALLS_MAP,        // 7
        ACCESS_FLOOR_MAP,        // 8
        DUNGEON_MAP,             // 9
        LOWER_DUNGEON_MAP,       // 10
        CATACOMB_MAP,            // 11
        LOWER_REACHES_MAP,       // 12
        WARRENS_MAP,             // 13
        HIDDEN_CAVERNS_MAP,      // 14
        FENSOFINSANITY_MAP,      // 15
        CHAOSCORRIDORS_MAP,      // 16
        LABYRINTH_MAP,           // 17
        HALLS_OF_BLOOD_MAP,      // 18
        NEMESISSLAIR_MAP,        // 19
        LASTMAP
    }
}
