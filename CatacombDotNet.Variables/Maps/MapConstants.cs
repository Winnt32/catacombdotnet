﻿namespace CatacombDotNet.Variables.Maps
{
    /// <summary>
    /// MAPSC3D.H
    /// </summary>
    public static class MapConstants
    {
        public const int ANIM = 402;
        /// <summary>
        /// ID_CA.H line 45
        /// </summary>
        public const int NUMMAPS = 30;
        /// <summary>
        /// ID_CA.H line 46
        /// </summary>
        public const int MAPPLANES = 3;
    }
}
