﻿namespace CatacombDotNet.Variables.Sound
{
    /// <summary>
    /// AUDIOC3D.H
    /// </summary>
    public enum Soundnames
    {
        HITWALLSND = 0,          // 0
        WARPUPSND,               // 1
        WARPDOWNSND,             // 2
        GETBOLTSND,              // 3
        GETNUKESND,              // 4
        GETPOTIONSND,            // 5
        GETKEYSND,               // 6
        GETSCROLLSND,            // 7
        GETPOINTSSND,            // 8
        USEBOLTSND,              // 9
        USENUKESND,              // 10
        USEPOTIONSND,            // 11
        USEKEYSND,               // 12
        NOITEMSND,               // 13
        WALK1SND,                // 14
        WALK2SND,                // 15
        TAKEDAMAGESND,           // 16
        MONSTERMISSSND,          // 17
        GAMEOVERSND,             // 18
        SHOOTSND,                // 19
        BIGSHOOTSND,             // 20
        SHOOTWALLSND,            // 21
        SHOOTMONSTERSND,         // 22
        TAKEDMGHURTSND,          // 23
        BALLBOUNCESND,           // 24
        COMPSCOREDSND,           // 25
        KEENSCOREDSND,           // 26
        COMPPADDLESND,           // 27
        KEENPADDLESND,           // 28
        NOWAYSND,                // 29
        LASTSOUND
    }
}
