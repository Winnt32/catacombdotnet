﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatacombDotNet.Variables.Graphics
{
    /// <summary>
    /// GFXE_C3D.H
    /// </summary>
    public static class GraphicConstants
    {
        //
        // Data LUMPs
        //
        public const int CONTROLS_LUMP_START = 5;
        public const int CONTROLS_LUMP_END = 20;
        public const int PADDLE_LUMP_START = 163;
        public const int PADDLE_LUMP_END = 165;
        public const int ORC_LUMP_START = 59;
        public const int ORC_LUMP_END = 68;
        public const int TROLL_LUMP_START = 69;
        public const int TROLL_LUMP_END = 79;
        public const int WARP_LUMP_START = 80;
        public const int WARP_LUMP_END = 83;
        public const int BOLT_LUMP_START = 84;
        public const int BOLT_LUMP_END = 85;
        public const int NUKE_LUMP_START = 86;
        public const int NUKE_LUMP_END = 87;
        public const int POTION_LUMP_START = 88;
        public const int POTION_LUMP_END = 88;
        public const int RKEY_LUMP_START = 89;
        public const int RKEY_LUMP_END = 89;
        public const int YKEY_LUMP_START = 90;
        public const int YKEY_LUMP_END = 90;
        public const int GKEY_LUMP_START = 91;
        public const int GKEY_LUMP_END = 91;
        public const int BKEY_LUMP_START = 92;
        public const int BKEY_LUMP_END = 92;
        public const int SCROLL_LUMP_START = 93;
        public const int SCROLL_LUMP_END = 93;
        public const int CHEST_LUMP_START = 94;
        public const int CHEST_LUMP_END = 94;
        public const int PLAYER_LUMP_START = 95;
        public const int PLAYER_LUMP_END = 98;
        public const int DEMON_LUMP_START = 99;
        public const int DEMON_LUMP_END = 109;
        public const int MAGE_LUMP_START = 110;
        public const int MAGE_LUMP_END = 115;
        public const int BAT_LUMP_START = 116;
        public const int BAT_LUMP_END = 121;
        public const int GREL_LUMP_START = 122;
        public const int GREL_LUMP_END = 132;
        public const int EXPWALL_LUMP_START = 134;
        public const int EXPWALL_LUMP_END = 136;
        public const int WALL1_LUMP_START = 137;
        public const int WALL1_LUMP_END = 138;
        public const int WALL2_LUMP_START = 139;
        public const int WALL2_LUMP_END = 140;
        public const int WALL3_LUMP_START = 141;
        public const int WALL3_LUMP_END = 142;
        public const int WALL4_LUMP_START = 143;
        public const int WALL4_LUMP_END = 144;
        public const int WALL5_LUMP_START = 145;
        public const int WALL5_LUMP_END = 146;
        public const int WALL6_LUMP_START = 147;
        public const int WALL6_LUMP_END = 148;
        public const int WALL7_LUMP_START = 149;
        public const int WALL7_LUMP_END = 150;
        public const int RDOOR_LUMP_START = 151;
        public const int RDOOR_LUMP_END = 152;
        public const int YDOOR_LUMP_START = 153;
        public const int YDOOR_LUMP_END = 154;
        public const int GDOOR_LUMP_START = 155;
        public const int GDOOR_LUMP_END = 156;
        public const int BDOOR_LUMP_START = 157;
        public const int BDOOR_LUMP_END = 158;


        //
        // Amount of each data item
        //
        public const int NUMCHUNKS = 478;
        public const int NUMFONT = 2;
        public const int NUMFONTM = 0;
        public const int NUMPICS = 155;
        public const int NUMPICM = 3;
        public const int NUMSPRITES = 3;
        public const int NUMTILE8 = 108;
        public const int NUMTILE8M = 36;
        public const int NUMTILE16 = 216;
        public const int NUMTILE16M = 72;
        public const int NUMTILE32 = 0;
        public const int NUMTILE32M = 0;
        public const int NUMEXTERNS = 22;
        //
        // File offsets for data items
        //
        public const int STRUCTPIC = 0;
        public const int STRUCTPICM = 1;
        public const int STRUCTSPRITE = 2;

        public const int STARTFONT = 3;
        public const int STARTFONTM = 5;
        public const int STARTPICS = 5;
        public const int STARTPICM = 160;
        public const int STARTSPRITES = 163;
        public const int STARTTILE8 = 166;
        public const int STARTTILE8M = 167;
        public const int STARTTILE16 = 168;
        public const int STARTTILE16M = 384;
        public const int STARTTILE32 = 456;
        public const int STARTTILE32M = 456;
        public const int STARTEXTERNS = 456;
    }
}
