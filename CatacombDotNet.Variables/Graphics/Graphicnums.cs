﻿namespace CatacombDotNet.Variables.Graphics
{
    // Заметки по EGA
    // Обычно игры использовали режим 320×200 w/16 colors, pixel aspect ratio of 1:1.2.
    // Видеопамять EGA состоит из 4 плоскостей RGBI равного размера (8 KB)
    // Все плоскости существуют параллельно по одному и томуже адресу - 0xa0000 

    // Во всех 16цветных графических режимах использовались все 4 плоскости,
    // каждая из которых хранила монохромную картинку для одного из 4 компонент
    // общего цвета — красного, зелёного, синего и интенсивности (в палитре по умолчанию).
    // Комбинированием битов из 4 плоскостей получалось 4битное значение цвета пиксела(индекса в палитру). 

    // ID_VW.C -> VWB_DrawPic, line 1398
    // GRMODE		=	EGAGR
    // ID_VW_AE.ASM - графические функции

    // ID_VW.C -> void VW_SetLineWidth (int width), line 376 (width = 40)

    // http://www.shikadi.net/moddingwiki/EGAGraph_Format
    // Структура EGAGRAPH.C3D
    // STRUCTPIC (Picture table, Chunk = 0) +
    // STRUCTPICM (Masked picture table, Chunk = 1) +
    // STRUCTSPRITE (Sprite table, Chunk = 2) +
    // STARTFONT (Font, Chunk = 3)
    // STARTFONT + 1 (Little Font, Chunk = 4)
    // STARTPICS (Pictures, Chunks 5 - 159) +
    // STARTPICM (Masked pictures, Chunks 160 - 162) + alpha channel +
    // STARTSPRITES (Sprites, Chunks 163 - 165) -> относятся к SCULL 'N' BONES +
    // STARTTILE8 (8x8 tiles, Chunk = 166) +
    // STARTTILE8M (8x8 masked tiles, Chunk = 167) + alpha channel
    // STARTTILE16 (16x16 tiles, Chunks 168 - 383) +
    // STARTTILE16M (16x16 masked tiles, Chunks 384 - 455) + alpha channel
    // STARTEXTERNS (externs, Chunks 456 - 477)

    /// <summary>
    /// GFXE_C3D.H
    /// </summary>
    public enum Graphicnums
    {
        // Lump Start
        CP_MAINMENUPIC = 5,
        CP_NEWGAMEMENUPIC,           // 6
        CP_LOADMENUPIC,              // 7
        CP_SAVEMENUPIC,              // 8
        CP_CONFIGMENUPIC,            // 9
        CP_SOUNDMENUPIC,             // 10
        CP_MUSICMENUPIC,             // 11
        CP_KEYBOARDMENUPIC,          // 12
        CP_KEYMOVEMENTPIC,           // 13
        CP_KEYBUTTONPIC,             // 14
        CP_JOYSTICKMENUPIC,          // 15
        CP_OPTIONSMENUPIC,           // 16
        CP_PADDLEWARPIC,             // 17
        CP_QUITPIC,                  // 18
        CP_JOYSTICKPIC,              // 19
        CP_MENUSCREENPIC,            // 20
        TITLEPIC,                    // 21
        CREDITSPIC,                  // 22
        HIGHSCORESPIC,               // 23
        FINALEPIC,                   // 24
        STATUSPIC,                   // 25
        SIDEBARSPIC,                 // 26
        SCROLLTOPPIC,                // 27
        SCROLL1PIC,                  // 28
        SCROLL2PIC,                  // 29
        SCROLL3PIC,                  // 30
        SCROLL4PIC,                  // 31
        SCROLL5PIC,                  // 32
        SCROLL6PIC,                  // 33
        SCROLL7PIC,                  // 34
        SCROLL8PIC,                  // 35
        FIRSTLATCHPIC,               // 36
        NOSHOTPOWERPIC,              // 37
        SHOTPOWERPIC,                // 38
        NOBODYPIC,                   // 39
        BODYPIC,                     // 40
        COMPAS1PIC,                  // 41
        COMPAS2PIC,                  // 42
        COMPAS3PIC,                  // 43
        COMPAS4PIC,                  // 44
        COMPAS5PIC,                  // 45
        COMPAS6PIC,                  // 46
        COMPAS7PIC,                  // 47
        COMPAS8PIC,                  // 48
        COMPAS9PIC,                  // 49
        COMPAS10PIC,                 // 50
        COMPAS11PIC,                 // 51
        COMPAS12PIC,                 // 52
        COMPAS13PIC,                 // 53
        COMPAS14PIC,                 // 54
        COMPAS15PIC,                 // 55
        COMPAS16PIC,                 // 56
        DEADPIC,                     // 57
        FIRSTSCALEPIC,               // 58
        ORC1PIC,                     // 59
        ORC2PIC,                     // 60
        ORC3PIC,                     // 61
        ORC4PIC,                     // 62
        ORCATTACK1PIC,               // 63
        ORCATTACK2PIC,               // 64
        ORCOUCHPIC,                  // 65
        ORCDIE1PIC,                  // 66
        ORCDIE2PIC,                  // 67
        ORCDIE3PIC,                  // 68
        TROLL1PIC,                   // 69
        TROLL2PIC,                   // 70
        TROLL3PIC,                   // 71
        TROLL4PIC,                   // 72
        TROLLOUCHPIC,                // 73
        TROLLATTACK1PIC,             // 74
        TROLLATTACK2PIC,             // 75
        TROLLATTACK3PIC,             // 76
        TROLLDIE1PIC,                // 77
        TROLLDIE2PIC,                // 78
        TROLLDIE3PIC,                // 79
        WARP1PIC,                    // 80
        WARP2PIC,                    // 81
        WARP3PIC,                    // 82
        WARP4PIC,                    // 83
        BOLTOBJPIC,                  // 84
        BOLTOBJ2PIC,                 // 85
        NUKEOBJPIC,                  // 86
        NUKEOBJ2PIC,                 // 87
        POTIONOBJPIC,                // 88
        RKEYOBJPIC,                  // 89
        YKEYOBJPIC,                  // 90
        GKEYOBJPIC,                  // 91
        BKEYOBJPIC,                  // 92
        SCROLLOBJPIC,                // 93
        CHESTOBJPIC,                 // 94
        PSHOT1PIC,                   // 95
        PSHOT2PIC,                   // 96
        BIGPSHOT1PIC,                // 97
        BIGPSHOT2PIC,                // 98
        DEMON1PIC,                   // 99
        DEMON2PIC,                   // 100
        DEMON3PIC,                   // 101
        DEMON4PIC,                   // 102
        DEMONATTACK1PIC,             // 103
        DEMONATTACK2PIC,             // 104
        DEMONATTACK3PIC,             // 105
        DEMONOUCHPIC,                // 106
        DEMONDIE1PIC,                // 107
        DEMONDIE2PIC,                // 108
        DEMONDIE3PIC,                // 109
        MAGE1PIC,                    // 110
        MAGE2PIC,                    // 111
        MAGEOUCHPIC,                 // 112
        MAGEATTACKPIC,               // 113
        MAGEDIE1PIC,                 // 114
        MAGEDIE2PIC,                 // 115
        BAT1PIC,                     // 116
        BAT2PIC,                     // 117
        BAT3PIC,                     // 118
        BAT4PIC,                     // 119
        BATDIE1PIC,                  // 120
        BATDIE2PIC,                  // 121
        GREL1PIC,                    // 122
        GREL2PIC,                    // 123
        GRELATTACKPIC,               // 124
        GRELHITPIC,                  // 125
        GRELDIE1PIC,                 // 126
        GRELDIE2PIC,                 // 127
        GRELDIE3PIC,                 // 128
        GRELDIE4PIC,                 // 129
        GRELDIE5PIC,                 // 130
        GRELDIE6PIC,                 // 131
        NEMESISPIC,                  // 132
        FIRSTWALLPIC,                // 133
        EXPWALL1PIC,                 // 134
        EXPWALL2PIC,                 // 135
        EXPWALL3PIC,                 // 136
        WALL1LPIC,                   // 137
        WALL1DPIC,                   // 138
        WALL2DPIC,                   // 139
        WALL2LPIC,                   // 140
        WALL3DPIC,                   // 141
        WALL3LPIC,                   // 142
        WALL4DPIC,                   // 143
        WALL4LPIC,                   // 144
        WALL5DPIC,                   // 145
        WALL5LPIC,                   // 146
        WALL6DPIC,                   // 147
        WALL6LPIC,                   // 148
        WALL7DPIC,                   // 149
        WALL7LPIC,                   // 150
        RDOOR1PIC,                   // 151
        RDOOR2PIC,                   // 152
        YDOOR1PIC,                   // 153
        YDOOR2PIC,                   // 154
        GDOOR1PIC,                   // 155
        GDOOR2PIC,                   // 156
        BDOOR1PIC,                   // 157
        BDOOR2PIC,                   // 158
        ENTERPLAQUEPIC,              // 159

        CP_MENUMASKPICM = 160,
        HAND1PICM,                   // 161
        HAND2PICM,                   // 162

        // Lump Start
        PADDLESPR = 163,
        BALLSPR,                     // 164
        BALL1PIXELTOTHERIGHTSPR,     // 165

        LEVEL1TEXT = 456,
        LEVEL2TEXT,                  // 457
        LEVEL3TEXT,                  // 458
        LEVEL4TEXT,                  // 459
        LEVEL5TEXT,                  // 460
        LEVEL6TEXT,                  // 461
        LEVEL7TEXT,                  // 462
        LEVEL8TEXT,                  // 463
        LEVEL9TEXT,                  // 464
        LEVEL10TEXT,                 // 465
        LEVEL11TEXT,                 // 466
        LEVEL12TEXT,                 // 467
        LEVEL13TEXT,                 // 468
        LEVEL14TEXT,                 // 469
        LEVEL15TEXT,                 // 470
        LEVEL16TEXT,                 // 471
        LEVEL17TEXT,                 // 472
        LEVEL18TEXT,                 // 473
        LEVEL19TEXT,                 // 474
        LEVEL20TEXT,                 // 475
        OUTOFMEM,                    // 476
        PIRACY,                      // 477
        ENUMEND
    }
}
