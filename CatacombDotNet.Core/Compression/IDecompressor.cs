﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatacombDotNet.Core.Compression
{
    public interface IDecompressor
    {
        void Decompress(byte[] srcBuffer, int srcOffset,  int srcCount, byte[] trgBuffer, int trgOffset, int trgCount);
    }
}
