﻿using System.IO;

namespace CatacombDotNet.Core.Factories
{
    public interface ISoundFactory
    {
        MemoryStream GetSound(int chunk);
        MemoryStream GetMusic(int chunk);
    }
}