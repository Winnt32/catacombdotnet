﻿namespace CatacombDotNet.Core.Resources
{
    public interface IChunkCompInfoGetter
    {
        ChunkInfo GetChunkInfo(int chunk, byte[] compChunk);
    }
}
