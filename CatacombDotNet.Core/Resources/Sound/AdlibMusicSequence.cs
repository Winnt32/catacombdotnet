﻿using System;

namespace CatacombDotNet.Core.Resources.Sound
{
    public struct AdlibMusicSequence
    {
        public readonly AdlibMusicEntry[] Values;

        public AdlibMusicSequence(byte[] buffer, int offset, int length)
        {
            var size = BitConverter.ToUInt16(buffer, offset);
            size /= 4;
            offset += 2;
            
            Values = new AdlibMusicEntry[size];

            for (var i = 0; i < Values.Length; i++)
            {
                Values[i] = new AdlibMusicEntry(buffer[offset], 
                    buffer[offset + 1], BitConverter.ToUInt16(buffer, offset + 2));

                offset += 4;
            }
        }
    }
}
