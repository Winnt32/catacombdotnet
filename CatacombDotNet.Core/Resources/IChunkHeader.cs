﻿namespace CatacombDotNet.Core.Resources
{
    public interface IChunkHeader
    {
        ChunkInfo GetChunkInfo(int chunk);
    }
}
