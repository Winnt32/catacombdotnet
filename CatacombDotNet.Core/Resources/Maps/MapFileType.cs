﻿using System;

namespace CatacombDotNet.Core.Resources.Maps
{
    //    typedef struct
    //{

    //unsigned RLEWtag;
    //long headeroffsets[100];
    //byte tileinfo[];
    //}
    //mapfiletype;

    /// <summary>
    /// ID.CA.C line 61
    /// </summary>
    public struct MapFileType
    {
        public readonly ushort RLEWtag;
        public readonly int[] HeaderOffsets;
        public readonly byte[] TileInfo;

        public MapFileType(byte[] buffer, int offest, int length)
        {
            RLEWtag = BitConverter.ToUInt16(buffer, offest);
            offest += 2;

            HeaderOffsets = new int[100];

            for (var i = 0; i < HeaderOffsets.Length; i++)
            {
                HeaderOffsets[i] = BitConverter.ToInt32(buffer, offest);
                offest += 4;
            }

            TileInfo = new byte[length - offest];

            Array.Copy(buffer, offest, TileInfo, 0, TileInfo.Length);
        }
    }
}
