﻿using System;
using System.Runtime.InteropServices;

namespace CatacombDotNet.Core.Resources.Graphics
{

    //    typedef struct
    //{
    //int width,
    //    height,
    //    orgx, orgy,
    //    xl, yl, xh, yh,
    //    shifts;
    //}
    //spritetabletype;

    /// <summary>
    /// ID_VW.H line 163
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct SpriteTable
    {
        public short Width;
        public short Height;
        public short Orgx;
        public short Orgy;
        public short Xl;
        public short Yl;
        public short Xh;
        public short Yh;
        public short Shifts;

        public SpriteTable(short width, short height, short orgx, short orgy,
            short xl, short yl, short xh, short yh, short shifts)
        {
            Width = width;
            Height = height;
            Orgx = orgx;
            Orgy = orgy;
            Xl = xl;
            Yl = yl;
            Xh = xh;
            Yh = yh;
            Shifts = shifts;
        }

        public static SpriteTable[] FromByteArray(byte[] buffer, int numsprites)
        {
            var spritesTable = new SpriteTable[numsprites];

            for (int i = 0; i < spritesTable.Length; i++)
            {
                spritesTable[i] = new SpriteTable(
                    BitConverter.ToInt16(buffer, i * 18),
                    BitConverter.ToInt16(buffer, i * 18 + 2),
                    BitConverter.ToInt16(buffer, i * 18 + 4),
                    BitConverter.ToInt16(buffer, i * 18 + 6),
                    BitConverter.ToInt16(buffer, i * 18 + 8),
                    BitConverter.ToInt16(buffer, i * 18 + 10),
                    BitConverter.ToInt16(buffer, i * 18 + 12),
                    BitConverter.ToInt16(buffer, i * 18 + 14),
                    BitConverter.ToInt16(buffer, i * 18 + 16)
                    );
            }

            return spritesTable;
        }

    }
}
