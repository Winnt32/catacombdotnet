﻿namespace CatacombDotNet.Core.Resources.Graphics
{
    public struct EgaChar
    {
        public readonly int Height;
        public readonly int Width;
        public readonly byte[] Data;

        public EgaChar(int height, int width, byte[] data)
        {
            Height = height;
            Width = width;
            Data = data;
        }
    }
}
