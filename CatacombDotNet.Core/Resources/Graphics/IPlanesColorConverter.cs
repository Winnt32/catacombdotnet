﻿namespace CatacombDotNet.Core.Resources.Graphics
{
    public interface IPlanesColorConverter
    {
        int ToColor(int blue, int green, int red, int intensity);
    }
}
