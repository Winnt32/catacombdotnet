﻿using System.Drawing;

namespace CatacombDotNet.Core.Resources.Graphics
{
    public interface IPictureConverter
    {
        void ToBitmap(byte[] source, int offset, int length, Bitmap bitmap);
        void ToMaskedBitmap(byte[] source, int offset, int length, Bitmap bitmap, Bitmap mask);
        void ToCharBitmap(byte[] source, int offset, int length, Bitmap bitmap);
    }
}
